//
//  UserTableCell.swift
//  GitHubUserTestCase
//
//  Created by iOS Developer on 02/11/2018.
//  Copyright © 2018 iOS Developer. All rights reserved.
//

import UIKit

class UserTableCell: UITableViewCell {

    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var infoActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var addInfoContainer: UIView!
    @IBOutlet weak var loadInfoBtn: Button!
    @IBOutlet weak var loadBtnCont: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        loadInfoBtn.didTouchUpInside = nil
    }
    
    func setInfoBlock(visible:Bool) {
        visible ? infoActivityIndicator.stopAnimating()
            : infoActivityIndicator.startAnimating()
        loadInfoBtn.isHidden = visible
        addInfoContainer.isHidden = !visible
        loadBtnCont.isHidden = visible
    }

}
