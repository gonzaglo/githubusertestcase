//
//  Button.swift
//  GitHubUserTestCase
//
//  Created by Георгий Кузьминых on 03/11/2018.
//  Copyright © 2018 labon. All rights reserved.
//

import UIKit

class Button: UIButton {

    typealias DidTapButton = (Button) -> ()
    
    var didTouchUpInside: DidTapButton? {
        didSet {
            if didTouchUpInside != nil {
                addTarget(self, action: #selector(actionDidTouchUpInside), for: .touchUpInside)
            } else {
                removeTarget(self, action: #selector(actionDidTouchUpInside), for: .touchUpInside)
            }
        }
    }
    
    @objc func actionDidTouchUpInside(sender: UIButton) {
        if let handler = didTouchUpInside {
            handler(self)
        }
    }

}
