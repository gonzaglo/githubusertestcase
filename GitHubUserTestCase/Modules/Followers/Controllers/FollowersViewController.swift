//
//  FollowersViewController.swift
//  GitHubUserTestCase
//
//  Created by iOS Developer on 02/11/2018.
//  Copyright © 2018 iOS Developer. All rights reserved.
//

import UIKit

class FollowersViewController: UIViewController {
    
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var imageLoaderSwitch: UIBarButtonItem!
    
    var viewModel:FollowersVMProtocol!
    
    private let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        loadUsers()
        imageLoaderSwitch.title = NetworkManager.shared.image.loaderName
        title = viewModel?.getLogin()
    }
    
    func configureTableView(){
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        let bottomIndicator = UIActivityIndicatorView(style: .gray)
        bottomIndicator.startAnimating()
        bottomIndicator.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
        tableView.tableFooterView = bottomIndicator
        tableView.tableFooterView?.isHidden = true
    }
    
    
    func loadUsers(refresh:Bool = false){
        viewModel?.loadUsers(refresh: refresh){[weak self] error in
            guard let `self` = self else {return}
            
            DispatchQueue.main.async {
                
                if let error = error {
                    self.alert(message: error.localizedDescription){
                        self.loadUsers()
                    }
                }
                
                self.tableView.tableFooterView?.isHidden = true
                self.refreshControl.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }
    
    func loadInfo(for user:User, at indexPath:IndexPath){
        
        viewModel?.loadInfo(for: user) {[weak self] error in
            guard let `self` = self else {return}
            
            DispatchQueue.main.async{
                
                self.tableView.reloadRows(at: [indexPath], with: .none)
                
                if let error = error {
                    
                    self.alert(message: error.localizedDescription) {
                        self.loadInfo(for: user, at: indexPath)
                        return
                    }
                    
                }
            }
        }
    }
    
    @objc private func refreshData(_ sender: Any) {
        loadUsers(refresh: true)
    }
    
    @IBAction func imageLoaderSwitchTap(_ sender: Any) {
        imageLoaderSwitch.title = NetworkManager.shared.image.switchLoader()
    }
    
}
