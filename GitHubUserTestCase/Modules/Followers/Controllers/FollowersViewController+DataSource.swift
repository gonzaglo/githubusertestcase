//
//  FollowersViewController+DataSource.swift
//  GitHubUserTestCase
//
//  Created by Георгий Кузьминых on 27/05/2019.
//  Copyright © 2019 labon. All rights reserved.
//

import UIKit

extension FollowersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getUsers().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeue(cell: UserTableCell.self, for: indexPath)
        let user = viewModel.getUsers()[indexPath.row]
        
        cell.loginLabel.text = user.login
        NetworkManager.shared.image.loadImage(by: user.avatarUrl, in: cell.avatarView)
        
        if let userInfo = user.info {
            cell.setInfoBlock(visible: true)
            cell.companyLabel.text = userInfo.company ?? "none"
            cell.nameLabel.text = userInfo.name ?? "none"
        }else{
            cell.setInfoBlock(visible: false)
            cell.loadInfoBtn.didTouchUpInside = {[weak self](sender) in
                cell.loadInfoBtn.isHidden = true
                self?.loadInfo(for: user, at: indexPath)
            }
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let viewModel = viewModel as? FollowersViewModel {
            let vc = viewModel.userInstance(for: indexPath.row)
            navigationController?.show(vc, sender: nil)
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == viewModel.getUsers().count - 1 {
            tableView.tableFooterView?.isHidden = false
            loadUsers()
        }
    }
    
}
