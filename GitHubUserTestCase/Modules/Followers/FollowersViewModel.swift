//
//  FollowersViewModel.swift
//  GitHubUserTestCase
//
//  Created by Георгий Кузьминых on 03/11/2018.
//  Copyright © 2018 labon. All rights reserved.
//

import UIKit
import NetKit

protocol FollowersVMProtocol {
    init(login:String)
    func loadUsers(refresh: Bool, completion: @escaping (_ error:Error?)->Void)
    func loadInfo(for user: User, completion: @escaping (_ error:Error?)->())
    func getUsers()->[User]
    func getLogin()->String
}

class FollowersViewModel:FollowersVMProtocol {
    
    private var users:[User]
    private var login:String
    private var page:Int
    private var isLoading:Bool
    private var allDataLoaded:Bool
    
    required init(login: String = Constant.defLogin) {
        self.login = login
        self.users = []
        self.page = 1
        self.isLoading = false
        self.allDataLoaded = false
    }
    
    func loadUsers(refresh: Bool, completion: @escaping (_ error:Error?)->Void) {
        
        if allDataLoaded || isLoading {return}
        isLoading = true
        
        if refresh {
            page = 1
        }
        
        NetworkManager.shared.user.getFollowers([User].self, by: login, at: page) { [weak self] users, error in
            guard let `self` = self else {return}
            
            guard let users = users as? [User] else {
                completion(error)
                self.isLoading = false
                return
            }
            
            
            if users.isEmpty {
                self.allDataLoaded = true
                self.isLoading = false
            }
            
            self.users.append(contentsOf: users)
            refresh ? (self.users = users) : (self.users += users)
            self.page += 1
            self.isLoading = false
            completion(nil)
            
        }
        
    }
    
    func loadInfo(for user: User, completion: @escaping (Error?) -> ()) {
        
        NetworkManager.shared.user.getInfo(AdditionalInfo.self, by: user.login) {userInfo, error in
            guard let userInfo = userInfo as? AdditionalInfo else {
                completion(error)
                return
            }
            user.info = userInfo
            completion(nil)
        }
        
    }
    
    func getUsers() -> [User]{
        return users
    }
    
    func getLogin() -> String {
        return login
    }
    
    static func userInstance(for login:String? = nil) -> FollowersViewController {
        let storyboard = UIStoryboard(name: "Followers", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing:FollowersViewController.self)) as! FollowersViewController
        controller.viewModel = FollowersViewModel(login: login ?? Constant.defLogin)
        return controller
    }
    
    func userInstance(for position:Int) -> FollowersViewController {
        let storyboard = UIStoryboard(name: "Followers", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: String(describing:FollowersViewController.self)) as! FollowersViewController
        controller.viewModel = FollowersViewModel(login: getUsers()[position].login)
        return controller
    }
}
