//
//  User.swift
//  GitHubUserTestCase
//
//  Created by iOS Developer on 02/11/2018.
//  Copyright © 2018 iOS Developer. All rights reserved.
//

import Foundation

class User:Codable {
    let login:String
    let avatarUrl:URL
    var info:AdditionalInfo?
    
    enum CodingKeys: String, CodingKey {
        case login
        case avatarUrl = "avatar_url"
    }
    
}

class AdditionalInfo:Codable{
    var company:String?
    var name:String?
}


