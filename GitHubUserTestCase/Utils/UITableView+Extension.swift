//
//  UITableView+Extension.swift
//  GitHubUserTestCase
//
//  Created by Георгий Кузьминых on 27/05/2019.
//  Copyright © 2019 labon. All rights reserved.
//

import UIKit

public extension UITableView {
    
    func register<T: UITableViewCell>(cell: T.Type) {
        let nib = UINib(nibName: String(describing: cell), bundle: nil)
        self.register(nib, forCellReuseIdentifier: String(describing: cell))
    }
    
    func dequeue<T: UITableViewCell>(cell name: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: name), for: indexPath) as? T else {
            fatalError("Couldn't find UITableViewCell for \(String(describing: name))")
        }
        return cell
    }
}

