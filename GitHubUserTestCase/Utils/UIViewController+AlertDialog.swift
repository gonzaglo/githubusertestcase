//
//  UIViewController+AlertDialog.swift
//  GitHubUserTestCase
//
//  Created by Георгий Кузьминых on 27/05/2019.
//  Copyright © 2019 labon. All rights reserved.
//

import UIKit

public extension UIViewController {
    func alert(message: String, againHandler:(()->())? = nil) {
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        if let againHandler = againHandler {
            let againAction = UIAlertAction(title: "Again", style: .default) { _ in
                againHandler()
            }
            alertController.addAction(againAction)
        }
        
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
