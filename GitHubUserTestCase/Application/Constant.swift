//
//  Constant.swift
//  GitHubUserTestCase
//
//  Created by Георгий Кузьминых on 03/11/2018.
//  Copyright © 2018 labon. All rights reserved.
//

import Foundation

class Constant{
    static let defLogin = "octocat"
    static let gitHubToken:String? = nil //Можно указать токен гитхаба для большего числа доступных запросов
}
