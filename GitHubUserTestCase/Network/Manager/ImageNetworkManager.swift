//
//  ImageNetworkManager.swift
//  GitHubUserTestCase
//
//  Created by Георгий Кузьминых on 03/11/2018.
//  Copyright © 2018 labon. All rights reserved.
//

import UIKit
import Kingfisher
import Nuke

class ImageNetworkManager {
    
    enum Loader:String{
        case kingfisher
        case nuke
        
        @discardableResult
        mutating func change() -> String{
            switch self {
            case .kingfisher:
                self = .nuke
            case .nuke:
                self = .kingfisher
            }
            return self.rawValue
        }
    }
    
    private var loader: Loader = .kingfisher
    var loaderName: String {
        return loader.rawValue
    }
    private let imageCache = NSCache<NSURL, UIImage>()
    
    private var defaulAvatar = UIImage(named: "avatar-default")
    
    
    func loadImage(by url:URL, in imageView: UIImageView?) {
        
        guard let imageView = imageView else {return}
        
        if let image = imageCache.object(forKey: url as NSURL) {
            imageView.image = image
            return
        }
        imageView.image = nil
        switch loader {
        case .kingfisher:
            ImageDownloader.default.downloadImage
            ImageDownloader.default.downloadImage(with: url, options: [], progressBlock: nil) {
                [weak imageView] result in
                guard let imageView = imageView else {return}
                
                switch result {
                case .success(let value):
                    self.saveToCache(image: value.image, for: url)
                    imageView.image = value.image
                case .failure(let error):
                    print("Error: \(error)")
                    imageView.image = self.defaulAvatar
                }
                
                
            }
            
        case .nuke:
            ImagePipeline.shared.loadImage(with: url,
                                           progress: nil,
                                           completion: { [weak imageView] response, _ in
                    guard let imageView = imageView else {return}
                    self.saveToCache(image: response?.image, for: url)
                    imageView.image = response?.image
                    
            })
            break
        }
    }
    
    func switchLoader() -> String{
        return loader.change()
    }
    
    private func saveToCache(image:UIImage?, for url:URL){
        guard let image = image else {return}
        imageCache.setObject(image, forKey: url as NSURL)
        print("download image by \(loader.rawValue)")
    }
    
}
