//
//  NetworkManager.swift
//  GitHubUserTestCase
//
//  Created by iOS Developer on 02/11/2018.
//  Copyright © 2018 iOS Developer. All rights reserved.
//

import Foundation

typealias ResultHandler = (Codable?, Error?) -> Void

enum APIError:Error {
    case authenticationError
    case notFound
    case badRequest
    case failed
    case noData
    case unableToDecode
    
    var localizedDescription: String {
        switch self {
        case .authenticationError: return "Token required. Specify in Constant.swift."
        case .notFound: return  "Not found"
        case .badRequest: return  "Bad request"
        case .failed: return  "Network request failed."
        case .noData: return  "Response returned with no data to decode."
        case .unableToDecode: return  "We could not decode the response."
        }
    }
}

enum Response<Error>{
    case success
    case failure(Error)
}


class NetworkManager {
    
    static let shared = NetworkManager()
    
    let user = UserNetworkManager()
    let image = ImageNetworkManager()
    
}

extension HTTPURLResponse {
    func handleResponse() -> Response<APIError>{
        switch self.statusCode {
        case 200...299: return .success
        case 400: return .failure(.badRequest)
        case 401...403: return .failure(.authenticationError)
        case 404: return .failure(.notFound)
        default: return .failure(.failed)
        }
    }
}

