//
//  UserNetworkManager.swift
//  GitHubUserTestCase
//
//  Created by iOS Developer on 02/11/2018.
//  Copyright © 2018 iOS Developer. All rights reserved.
//

import Foundation


class UserNetworkManager {
    
    let router = Router<UserApi>()
    
    func getFollowers<T:Codable>(_ type: T.Type, by login:String,
                                 at page:Int,
                                 completion: @escaping ResultHandler) {
        
        router.request(.followers(login: login, page: page)) { data, error in
            self.fetch(to: type, data, error) { data, error in
                completion(data, error)
            }
        }
    }
    
    func getInfo<T:Codable>(_ type: T.Type, by login:String, completion: @escaping ResultHandler){
        
        router.request(.info(login: login)) { data, error in
            self.fetch(to: type, data, error) { data, error in
                completion(data, error)
            }
        }
    }
    
}

extension UserNetworkManager: FetchProtocol {
    func fetch<T:Codable>(to type: T.Type, _ data: Data?, _ error: Error?, completion: ResultHandler) {
        
        guard let responseData = data else {
            completion(nil, error ?? APIError.noData)
            return
        }
        
        do {
            print(responseData)
            let info = try JSONDecoder().decode(T.self, from: responseData)
            completion(info,nil)
        }catch {
            print(error)
            completion(nil, APIError.unableToDecode)
        }
        
        
    }
}
