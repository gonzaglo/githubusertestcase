//
//  UserGitHubEndPoint.swift
//  GitHubUserTestCase
//
//  Created by iOS Developer on 02/11/2018.
//  Copyright © 2018 iOS Developer. All rights reserved.
//

import Foundation

public enum UserApi {
    case followers(login:String, page:Int)
    case info(login:String)
}

extension UserApi: EndPointType {
    
    var baseURL: URL {
        guard let url = URL(string: "https://api.github.com/users/") else {
            fatalError("baseURL could not be configured.")
        }
        return url
    }
    
    var path: String {
        switch self {
        case .followers(let login, _):
            return "\(login)/followers"
        case .info(let login):
            return login
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .followers( _, let page):
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["page":page])
        case .info(_):
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        if let token = Constant.gitHubToken {
            return ["Authorization":"token \(token)"]
        }
        return nil
        
    }
}
