//
//  EndPointType.swift
//  GitHubUserTestCase
//
//  Created by iOS Developer on 02/11/2018.
//  Copyright © 2018 iOS Developer. All rights reserved.
//

import Foundation

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}
